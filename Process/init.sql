create database clickcollect;
use clickcollect;

create table produit (
    id int not null auto_increment primary key,
    nom varchar(800),
    imgURL varchar(800),
    prix float,
    dispo bool
);

create table info_ferme (
    adresse varchar (800),
    telephone varchar(800),
    horaires varchar (800)
);

create table client (
id int not null auto_increment primary key,
nom varchar (800),
email varchar (800),
telephone varchar (800)
);

create table commande (
    id int not null auto_increment primary key
    id_client int, -- ici on stock l'id du client qui commande
    foreign key (id_client) references client(id)
);

create table ligne_commande (
    id_produit int,  
    id_commande int not null,
    quantite float
);

create table etat_commande (
    panier enum (rempli, vide),
    validee enum (oui, non),
    prete enum (prêt, vide),
    collectee enum (non, oui)
);